require('dotenv').config();
const request = require('request');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const rota = require('path').basename(__filename, '.js');
var multer = require('multer');
var upload = multer();
let lista = [];


module.exports = async function (app) {

    app.use(cookieParser());
    app.use(session({
        secret: "2C44-4D44-WppQ38S"
    }));

    app.use(require('connect-flash')());
    app.use(function (req, res, next) {
        res.locals.messages = require('express-messages')(req, res);
        next();
    });

    // Rota 1 (para exibição da View Listar)
    app.get('/app/' + rota + '/list', function (req, res) {

        if (!req.session.token) {
            res.redirect('/app/login');
        } else {
            teste = request({
                url: process.env.API_HOST + rota,
                method: "GET",
                json: true,
                headers: {
                    "content-type": "application/json",
                    "Authorization": req.session.token
                },
            }, function (error, response, body) {
                lista = [];                
                for (var i = 0; i < Object.keys(body.dados).length; i++) {
                    const finallista = {
                       //Alaterar aqui 
                        bairro: body.dados[i].bairro,
                        cartaosus: body.dados[i].cartaosus,
                        cep: body.dados[i].cep,
                        cidade: body.dados[i].cidade,
                        complemento: body.dados[i].complemento,
                        cpf: body.dados[i].cpf,
                        datacadastro: body.dados[i].datacadastro,
                        datanascimento: body.dados[i].datanascimento,
                        id: body.dados[i].id,
                        logradouro: body.dados[i].logradouro,
                        mae: body.dados[i].mae,
                        descricao: body.dados[i].materiais.descricao,
                        id: body.dados[i].materiais.id,
                        nome: body.dados[i].nome,
                        numero: body.dados[i].numero,
                        pai: body.dados[i].pai,
                        rg: body.dados[i].rg,
                        sexo: body.dados[i].sexo,
                        telefone1: body.dados[i].telefone1,
                        telefone2: body.dados[i].telefone2
                    };
                    lista.push(finallista);
                }
                res.format({
                    html: function () {
                        res.render(rota + '/List', { itens: lista, page: rota });
                    }
                });
                return lista;
            });
        }
    });

    // Rota 2 (para exibição da View Criar)
    app.get('/app/' + rota + '/create', function (req, res) {
        if (!req.session.token) {
            res.redirect('/app/login');
        } else {
            res.format({
                html: function () {
                    res.render(rota + '/Create', { page: rota });
                }
            });
        }
    });

    // Rota 3 (para receber parametros via post criar item)
    app.post('/app/' + rota + '/create/submit', function (req, res) {


        request({
            url: process.env.API_HOST + rota,
            method: "POST",
            json: true,
            headers: {
                "content-type": "application/json",
                "Authorization": req.session.token
            },
            json: {
                 //Alaterar aqui 
                "bairro": req.body.bairro,
                "cartaosus": req.body.cartaosus,
                "cep": req.body.cep,
                "cidade": req.body.cidade,
                "complemento": req.body.complemento,
                "cpf": req.body.cpf,
                "datacadastro": req.body.datacadastro,
                "datanascimento": req.body.datanascimento,
                "id": req.body.id,
                "logradouro": req.body.logradouro,
                "mae": req.body.mae,
                "descricao": req.body.materiais.descricao,
                "id": req.body.materiais.id,
                "nome": req.body.nome,
                "numero": req.body.numero,
                "pai": req.body.pai,
                "rg": req.body.rg,
                "sexo": req.body.sexo,
                "telefone1": req.body.telefone1,
                "telefone2": req.body.telefone2,
                
            },
        }, function (error, response, body) {            

            if (response.statusCode != 200) {
                req.flash("danger", "Item não salvo. "+body.errors);
            } else {
                req.flash("success", "Item salvo com sucesso.");
            }

            res.redirect('/app/' + rota + '/list');
            return true;
        });

    });

    // Rota 4 (para exibição da View Editar)
    app.get('/app/' + rota + '/edit/:id', function (req, res) {
        if (!req.session.token) {
            res.redirect('/app/login');
        } else {
            request({
                url: process.env.API_HOST + rota + "/" + req.params.id,
                method: "GET",
                json: true,
                headers: {
                    "content-type": "application/json",
                    "Authorization": req.session.token
                },
            }, function (error, response, body) {
                res.format({
                    html: function () {
                        res.render(rota + '/Edit', {
                             //Alaterar aqui 
                            bairro: body.dados.bairro,
                            cartaosus: body.dados.cartaosus,
                            cep: body.dados.cep,
                            cidade: body.dados.cidade,
                            complemento: body.dados.complemento,
                            cpf: body.dados.cpf,
                            datacadastro: body.dados.datacadastro,
                            datanascimento: body.dados.datanascimento,
                            id: body.dados.id,
                            logradouro: body.dados.logradouro,
                            mae: body.dados.mae,
                            descricao: body.dados.materiais.descricao,
                            id: body.dados.materiais.id,
                            nome: body.dados.nome,
                            numero: body.dados.numero,
                            pai: body.dados.pai,
                            rg: body.dados.rg,
                            sexo: body.dados.sexo,
                            telefone1: body.dados.telefone1,
                            telefone2: body.dados.telefone2,
                            page: rota
                        });
                    }
                });
            });
        }
    });

    // Rota 5 (para receber parametros via post editar item)
    app.post('/app/' + rota + '/edit/submit', function (req, res) {

        json = req.body;
        request({
            url: process.env.API_HOST + rota,
            method: "PUT",
            json: true,
            headers: {
                "content-type": "application/json",
                "Authorization": req.session.token
            },
            json: {
                 //Alaterar aqui 
                "bairro": req.body.bairro,
                "cartaosus": req.body.cartaosus,
                "cep": req.body.cep,
                "cidade": req.body.cidade,
                "complemento": req.body.complemento,
                "cpf": req.body.cpf,
                "datacadastro": req.body.datacadastro,
                "datanascimento": req.body.datanascimento,
                "id": req.body.id,
                "logradouro": req.body.logradouro,
                "mae": req.body.mae,
                "descricao": req.body.materiais.descricao,
                "id": req.body.materiais.id,
                "nome": req.body.nome,
                "numero": req.body.numero,
                "pai": req.body.pai,
                "rg": req.body.rg,
                "sexo": req.body.sexo,
                "telefone1": req.body.telefone1,
                "telefone2": req.body.telefone2,
            },
        }, function (error, response, body) {

            if (response.statusCode != 200) {
                req.flash("danger", "Item não atualizado. "+body.errors);
            } else {
                req.flash("success", "Item atualizado com sucesso.");
            }

            res.redirect('/app/' + rota + '/list');
            return true;
        });

    });

    // Rota 6 (para exclusão de um item)
    app.post('/app/' + rota + '/delete/', function (req, res) {
        if (!req.session.token) {
            res.redirect('/app/login');
        } else {
            request({
                url: process.env.API_HOST + rota + "/" + req.body.id,
                method: "DELETE",
                json: true,
                headers: {
                    "content-type": "application/json",
                    "Authorization": req.session.token
                },
            }, function (error, response, body) {

                if (response.statusCode != 200) {
                    req.flash("danger", "Item não excluído. "+body.errors);
                } else {
                    req.flash("success", "Item excluído com sucesso.");
                }

                res.redirect('/app/' + rota + '/list');
                return true;
            });

        }
    });

}
