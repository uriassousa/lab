require('dotenv').config();
const request = require('request');
const cookieParser = require('cookie-parser');
const session = require('express-session');

module.exports = async function(app) {
    app.use(cookieParser());
    app.use(session({ secret: "2C44-4D44-WppQ38S" }));

    app.get('/app/login/', function(req, res) {
        res.format({
            html: function() {
                res.render('login');
            }
        });
    });

    app.post('/app/authentication/', function(req, res) {

        request({
            url: process.env.API_HOST_LOGIN,
            method: "POST",
            json: true,
            headers: {
                "content-type": "application/json"
            },
            json: {
                "password": req.body.password,
                "username": req.body.email,
            },
        }, function(error, response, body) {
             req.session.token = body.accessToken;
             res.redirect('/');
            return true;
        });

    });

    app.get('/app/sair', function(req, res) {
        req.session.destroy();
        res.redirect('/app/login');
    });

}
